package Exercicio3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import Exercicio2.Conta;


public class AgenciaViagens {
	
	static Scanner sc = new Scanner(System.in);

	static ArrayList<Lugar> destinos = new ArrayList<Lugar>();

	static Set<String> CPFclientes = new HashSet<String>();
	
	private static void AdicionarViagem() {

		System.out.println("Digite o destino da sua viagem: ");
		String destino = sc.nextLine();

		destinos.add(new Lugar(destino));
		
	}

	private static void ListarDadosDestino() {

		int i = 0;
	    for (Lugar ds: destinos) {
	      System.out.printf("Posi��o %d- %s\n", i, destinos);
	      i++;
	      System.out.println();
	    }
	
	}

	private static void ListarDadosPessoa(String CPF) {
		
		int i = 0;
	    for (String cpf: CPFclientes) {
	      System.out.printf("Posi��o %d- %s\n", i, cpf);
	      i++;
	      System.out.println();
	    }

	}

	public static void main(String[] args) {
		
		CPFclientes.add("1234");
		
		AdicionarViagem();
		
		ListarDadosDestino();
		ListarDadosPessoa("1234");
		
		sc.close();
	}

}
