package Exercicio2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class AgenciaBancaria {

	static Scanner sc = new Scanner(System.in);

	static ArrayList<Conta> contas = new ArrayList<Conta>();

	static Set<String> CPFclientes = new HashSet<String>();

	private static void AdicionarConta() {
		System.out.println("Digite o numero da conta que deseja adicionar: ");
		int conta = sc.nextInt();

		contas.add(new Conta(conta));

	}

	private static void ApagarConta() {

		System.out.println("Digite o numero da conta que deseja apagar: ");
		int conta = sc.nextInt();

		if (contas.contains(conta)) {
		    contas.remove(conta);
		}
	
	}

	private static void ListarDadosPessoa(String CPF) {

		int i = 0;
	    for (String cpf: CPFclientes) {
	      System.out.printf("Posi��o %d- %s\n", i, cpf);
	      i++;
	      System.out.println();
	    }

	}

	public static void main(String[] args) {
		
		CPFclientes.add("1234");
		

		AdicionarConta();
		System.out.println(contas);

		ApagarConta();
		System.out.println(contas);
		
		ListarDadosPessoa("1234");

		sc.close();

	}

}
