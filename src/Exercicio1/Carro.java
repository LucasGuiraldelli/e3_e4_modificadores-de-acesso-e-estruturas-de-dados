package Exercicio1;

public class Carro {
	
	private String modelo;
	private Integer placa;
	private Double preco;
	

	public Carro(String modelo, Integer placa, Double preco) {
		this.modelo = modelo;
		this.placa = placa;
		this.preco = preco;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Integer getPlaca() {
		return placa;
	}
	public void setPlaca(Integer placa) {
		this.placa = placa;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	
	

}
