package Exercicio1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Concessionaria {
	
	static ArrayList<String> car = new ArrayList<String>();
	
	static Map<String,Double> placa_preco = new HashMap<String,Double>();
	
	
	
	private static void ListarCarro() {
		
		int i = 0;
	    for (String carros: car) {
	      System.out.printf("Posi��o %d- %s\n", i, carros);
	      i++;
	      System.out.println();
	    }
		
	}
	
	private static void ImprimirPreco() {
		placa_preco.entrySet().stream().forEach(e -> {
			System.out.println("Placa: " + e.getKey() + " Pre�o: " + e.getValue());
			System.out.println();
		});
		
	}
	
	
	
	
	public static void main(String[] args) {
		
		
		// ArrayList
		car.add("fiesta");
		car.add("gtR");
		car.add("Ka");
		car.add("subaru");
		
		//HashMap
		placa_preco.put("ABC900", 20.000);
		placa_preco.put("CVF340", 50.000);
		placa_preco.put("OIU109", 60.000);
		placa_preco.put("UKL087", 90.000);
		
		
		
		ListarCarro(); 
		ImprimirPreco();
		
		

		
	}

}
